#!/usr/bin/env python3

import yaml
import os
import subprocess

conf_file = "user.yml"

if __name__ == "__main__":

    with open(conf_file, 'r') as stream:
        cfg = yaml.safe_load(stream)

    try:
        print( "'first_name': First name is a string", str(cfg['first_name'] ) )
        print( "'gitlab_name': gitlab_name is a string", str(cfg['gitlab_name'] ) )

        # git log -1 --pretty=%an 
        out = subprocess.Popen(['git', 'log', '-1', '--pretty=%an'],
                stdout=subprocess.PIPE,
                stderr=subprocess.STDOUT)
        res = out.communicate()[0].rstrip().decode('utf-8')
        if not res in str(cfg['gitlab_name']):
                raise ValueError("gitlab_name not corresponding to last commit of user.yml: '%s' vs.'%s'"
                     %(cfg['gitlab_name'], res))
        print( "'gitlab_name': commit name is", str(cfg['gitlab_name'] ) )

    except Exception as ex:
        print( "Config file error")
        print( "Exception was: ", ex )
        exit(1)

    exit(0)


